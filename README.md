# Demo Restful API using Flask, Flask-RESTPlus and Swagger UI

Flask enables exposure of Python functions as APIs. Flask-RESTPlus is an extension to Flask which allows we define the Restful API with the Swagger UI is integrated for all the APIs.

In this demo, we will develope a Flask application with several APIs and dummy data.

## Test the application

Start flask app with pipenv

```shell
pipenv shell
FLASK_APP=app.py flask run
```

Try to test it with Swagger UI here:

http://127.0.0.1:5000/